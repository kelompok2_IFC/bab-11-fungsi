#include <stdio.h>
#include <conio.h>
#include <math.h>

int phytagoras(int a, int b, int c){
	if(pow(c,2) == pow(a,2) + pow(b,2)){
		printf("\nTripel Phytagoras");
	}
	else {
		printf("\nBukan Tripel Phytagoras");
	}
	return pow(c,2) == pow(a,2) + pow(b,2);
}
int main(){
	int alas, tinggi, miring;
	
	printf("\nMasukkan alas : ");
	scanf("%d", &alas);
	printf("\nMasukkan tinggi : ");
	scanf("%d", &tinggi);
	printf("\nMasukkan miring : ");
	scanf("%d", &miring);
	
	phytagoras(alas, tinggi, miring);
	getch();
}
